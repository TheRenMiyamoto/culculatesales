package jp.alhinc.miyamoto_ren.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {
	public static  void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//Key:支店番号 Value:支店名
		Map <String, String> branchNames = new HashMap <String, String>();
		//Key:商品番号 Value:商品名
		Map <String, String> commodityNames = new HashMap <String, String>();
		//Key:支店番号 Value:売上額
		Map <String,  Long> branchSales = new HashMap <String, Long>();
		//Key:商品番号 Value:売上額
		Map <String,  Long> commoditySales = new HashMap <String, Long>();


		String branchChk = "[0-9]{3}";
		String commodityChk = "^[A-Za-z0-9]+$";
		String branchDef = "支店定義";
		String commodityDef = "商品定義";

		if(!inputFile(args[0], "branch.lst", branchDef, branchChk, branchNames, branchSales)) {
			return;
		}

		if(!inputFile(args[0], "commodity.lst", commodityDef, commodityChk, commodityNames, commoditySales)) {
			return;
		}



		//売上ファイルの読み込み
		File dir = new File(args[0]);
		File[] files = dir.listFiles();
		List <File> fileLists = new ArrayList <File>();
		Collections.sort(fileLists);
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			//ファイルなのか確認する方法
			if(files[i].isFile() && fileName.matches("[0-9]{8}.rcd$")) {
				fileLists.add(files[i]);
			}
		}

		//売上ファイルが連番か確認するエラー処理
		for(int i = 0; i < fileLists.size() -1; i++) {
			String fileName = fileLists.get(i).getName();
			String fileNameNxt = fileLists.get(i+1).getName();
			int former = Integer.parseInt(fileName.substring(0,8));
			int latter = Integer.parseInt(fileNameNxt.substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		for(int i = 0; i < fileLists.size(); i++) {
			try {
				FileReader fr = new FileReader(fileLists.get(i));
				br = new BufferedReader(fr);

				String line;
				ArrayList <String> list = new ArrayList <String>();
				while((line = br.readLine()) != null) {
					list.add(line);
				}

				//売上ファイルのフォーマットを確認する
				if(list.size() != 3) {
					System.out.println(fileLists.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				//売上金額が数字なのか確認する方法
				if(list.get(2).matches("[^0-9]")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//Mapに特定のKeyが存在するか確認する
				if(!branchNames.containsKey(list.get(0))) {
					System.out.println(fileLists.get(i).getName() + "の支店コードが不正です");
					return;
				}
				//商品コードが商品定義ファイルになかった場合
				if(!commodityNames.containsKey(list.get(1))){
					System.out.println(fileLists.get(i).getName() + "の商品コードが不正です");
					return;
				}

				long fileSales = Long.parseLong(list.get(2));
				long saleAmountB = branchSales.get(list.get(0)) + fileSales;
				long saleAmountC = commoditySales.get(list.get(1)) + fileSales;

				//売上金額の合計が10桁を超えたか確認する
				if(saleAmountB >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売上金額の合計が10桁を超えたか確認する
				if(saleAmountC >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(list.get(0),saleAmountB);
				commoditySales.put(list.get(1),saleAmountC);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}

		if(!outputFile(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		if(!outputFile(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	public static boolean inputFile(String path, String fileName, String fileDef, String regex, Map<String, String> names, Map<String, Long> sales) {


		//支店定義&商品定義ファイルの読み込み
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);
			//ファイルの存在を確認
			if(!file.exists()) {
				System.out.println(fileDef + "ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] fileInfo = line.split(",");
				//ファイルのフォーマットを確認する
				if((fileInfo.length != 2) || (!fileInfo[0].matches(regex))) {
					System.out.println(fileDef + "ファイルのフォーマットが不正です");
					return false;
				}
				names.put(fileInfo[0], fileInfo[1]);
				sales.put(fileInfo[0], (long) 0);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}


	public static boolean outputFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			File oFile = new File(path, fileName);
			FileWriter fw = new FileWriter(oFile);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()) {
				String branches = names.get(key);
				Long salesVal = sales.get(key);
				bw.write(key + "," + branches + "," + salesVal.toString());
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}
}

